// Step 1: Import React
import * as React from 'react'
// import { Link } from 'gatsby'
import Layout from '../components/layout'

// Step 2: Define your component
const AboutPage = () => {
    return (
      <Layout pageTitle="About Us">
        <p >
        We are a homeschooling family of 4. We are currently working on our website. We will be sharing our homeschooling journey with you. We will be sharing our curriculum, our daily schedule, our homeschooling space, and much more. We will also be sharing our family adventures. 
      </p>
      </Layout>
    )
  }
  

// Step 3: Export your component
export const Head = () => (
    <>
      <title>About Me</title>
      <meta name="description" content="Your description" />
    </>
  )
export default AboutPage